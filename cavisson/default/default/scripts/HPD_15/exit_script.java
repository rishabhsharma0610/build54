/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: cavisson
    Date of recording: 08/10/2021 12:19:28
    Flow details:
    Build details: 4.6.2 (build# 1)
    Modification History:
-----------------------------------------------------------------------------*/

package HPD_15;
import pacJnvmApi.NSApi;

public class exit_script
{
    public static int execute(NSApi nsApi)
    {
        return 0;
    }
}
