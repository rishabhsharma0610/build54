/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 08/10/2021 03:56:12
    Flow details:
    Build details: 4.6.2 (build# 1)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index_html");
    ns_web_url ("index_html",
        "URL=http://10.10.30.15:9014/tours/index.html",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("index_html", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("json");
    ns_web_url ("json",
        "URL=https://update.googleapis.com/service/update2/json?cup2key=10:3059518785&cup2hreq=24ecf808c6308302588bf807ceea37b542fbc566fef6f0f3edacf6c85197c04c",
        "METHOD=POST",
        "HEADER=X-Goog-Update-AppId:obedbbhbpmojnkanicioggnmelmoomoc,aemomkdncapdnfajjbbcbdebjljbpmpj,gcmjkmgdlgnkkcocmoeiminaijmmjnii,giekcmmlnklenlaomppkphknjmnnpneh,cmahhnpholdijhjokonmfdjbfmklppij,ggkkehgbnfjpeggfpleeakpidbkibbmn,khaoiebndkojlmppeemjhbpbandiljpe,ojhpjlocmbogdgmfpkhlaaeamibhnphh,jamhcnnkihinmdlkakkaopbjbbcngflc,llkgjffcdpffmhiakmfcdcblohccpfmo,jflookgnkcckhobaglndicnbbgbonegd,oimompecagnajdejgnnjijobebaeigek,hfnkpimlhhgieaddgfemjhofmfblmnib,pdafiollngonhoadbmdoemagnfpdphbe,eeigpngbgcognadeebkilcpcaedhellh",
        "HEADER=X-Goog-Update-Interactivity:bg",
        "HEADER=X-Goog-Update-Updater:chromium-90.0.4430.93",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chromium","acceptformat":"crx2,crx3","app":[{"accept_locale":"ENUS","appid":"obedbbhbpmojnkanicioggnmelmoomoc","cohort":"1:s6f:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.ce90443029e519576f7a3de55fe2a28284ad3fc73d9f63585bf28d8404ac2ae3"}]},"ping":{"ping_freshness":"{7e4c87d0-b325-4398-895d-dca6f9a1ec7e}","rd":5334},"updatecheck":{},"version":"20210717.385693513"},{"appid":"aemomkdncapdnfajjbbcbdebjljbpmpj","cohort":"1::","enabled":true,"ping":{"ping_freshness":"{64f9fd86-0982-4d45-85a4-ce9d983a3990}","rd":5334},"updatecheck":{},"version":"1.0.5.0"},{"appid":"gcmjkmgdlgnkkcocmoeiminaijmmjnii","cohort":"1:bm1:10q3@0.01,10ul@0.1","cohorthint":"M54ToM99","cohortname":"M54ToM99","enabled":true,"packages":{"package":[{"fp":"1.91ee417000553ca22ed67530545c4177a08e7ffcf602c292a71bd89ecd0568a5"}]},"ping":{"ping_freshness":"{3cc65bbd-e605-49ff-9111-12e657e0012e}","rd":5334},"updatecheck":{},"version":"9.28.0"},{"appid":"giekcmmlnklenlaomppkphknjmnnpneh","cohort":"1:j5l:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.fd515ec0dc30d25a09641b8b83729234bc50f4511e35ce17d24fd996252eaace"}]},"ping":{"ping_freshness":"{e3b0f5d0-4f5a-4c75-8433-69f170f533b3}","rd":5334},"updatecheck":{},"version":"7"},{"appid":"cmahhnpholdijhjokonmfdjbfmklppij","cohort":"1:wr3:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.b4ddbdce4f8d5c080328aa34c19cb533f2eedec580b5d97dc14f74935e4756b7"}]},"ping":{"ping_freshness":"{383b96a3-be4b-41da-a87e-395cff13996b}","rd":5334},"updatecheck":{},"version":"1.0.6"},{"appid":"ggkkehgbnfjpeggfpleeakpidbkibbmn","cohort":"1:ut9:","cohorthint":"M80ToM99","cohortname":"M80ToM99","enabled":true,"packages":{"package":[{"fp":"1.74566c4cadcd80a841c10f0636c336a7bb1b94f31a05d7e45ae8b385bfbf53ff"}]},"ping":{"ping_freshness":"{57cec759-f038-4be1-84df-04d327ec13f1}","rd":5334},"updatecheck":{},"version":"2021.7.19.1143"},{"appid":"khaoiebndkojlmppeemjhbpbandiljpe","cohort":"1:cux:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.ffd1d2d75a8183b0a1081bd03a7ce1d140fded7a9fb52cf3ae864cd4d408ceb4"}]},"ping":{"ping_freshness":"{fcb854bb-ed62-4493-8d65-2a84a3519030}","rd":5334},"updatecheck":{},"version":"43"},{"appid":"ojhpjlocmbogdgmfpkhlaaeamibhnphh","cohort":"1:w0x:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.478aa915e78878e332a0b4bb4d2a6fb67ff1c7f7b62fe906f47095ba5ae112d0"}]},"ping":{"ping_freshness":"{8feda3ea-5662-40e0-848f-08ad27fbf45c}","rd":5334},"updatecheck":{},"version":"1"},{"appid":"jamhcnnkihinmdlkakkaopbjbbcngflc","cohort":"1:wvr:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.a76cdce1c1cd7303a553d24d72b1d15dc2c3cc4f405ca649f4abf15dde311551"}]},"ping":{"ping_freshness":"{578597eb-9bdb-4b4f-9238-f7febb383588}","rd":5334},"updatecheck":{},"version":"94.0.4588.0"},{"appid":"llkgjffcdpffmhiakmfcdcblohccpfmo","cohort":"1::","enabled":true,"packages":{"package":[{"fp":"1.2731bdeddb1470bf2f7ae9c585e7315be52a8ce98b8af698ece8e500426e378a"}]},"ping":{"ping_freshness":"{9b0e876c-bb71-4a4b-885d-8acb3d49f6e3}","rd":5334},"updatecheck":{},"version":"1.0.0.8"},{"appid":"jflookgnkcckhobaglndicnbbgbonegd","cohort":"1:s7x:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.c963f6d51cdf6ffd8352f0a7feeb3a5bde0cbfdcf1cdb7598ba8b58f06bf0207"}]},"ping":{"ping_freshness":"{c35a02a1-180e-49a6-81be-696e7e3389fa}","rd":5334},"updatecheck":{},"version":"2657"},{"appid":"oimompecagnajdejgnnjijobebaeigek","cohort":"1::","enabled":true,"packages":{"package":[{"fp":"1.0727b38159b38ffa3633510444ece15c86417962e8cac59c59002f13b50239ac"}]},"ping":{"ping_freshness":"{d0c41566-bfbd-4fc0-b00b-c1971246127a}","rd":5334},"updatecheck":{},"version":"4.10.2209.0"},{"appid":"hfnkpimlhhgieaddgfemjhofmfblmnib","cohort":"1:jcl:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.73c7f62b7a6c69b828c90a2991f8ea4cd7ab786da56f8fabc9201d813c58a1bb"}]},"ping":{"ping_freshness":"{42c4620f-767b-4ee0-8ad5-52435db9b086}","rd":5334},"updatecheck":{},"version":"6755"},{"appid":"pdafiollngonhoadbmdoemagnfpdphbe","cohort":"1:vz3:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.baeb7c645c7704139756b02bf2741430d94ea3835fb1de77fef1057d8c844655"}]},"ping":{"ping_freshness":"{72109255-7530-46e4-9711-d14babd4325f}","rd":5334},"updatecheck":{},"version":"2021.2.22.1142"},{"appid":"eeigpngbgcognadeebkilcpcaedhellh","cohort":"1:w59:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.c64c9c1008f3ba5f6e18b3ca524bc98dcd8acfae0a2720a8f1f3ef0f8d643d05"}]},"ping":{"ping_freshness":"{62dc8309-18e6-4e3a-976e-b34634d34807}","rd":5334},"updatecheck":{},"version":"2020.11.2.164946"}],"arch":"x64","dedup":"cr","domainjoined":false,"hw":{"physmemory":12},"lang":"en-US","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.18363.1679"},"prodversion":"90.0.4430.93","protocol":"3.1","requestid":"{b80b06f5-fca8-44f4-b201-8fe8c919ed99}","sessionid":"{bd445761-9316-46a3-b2b5-19150e833c14}","updaterversion":"90.0.4430.93"}}",
        BODY_END
    );

    ns_end_transaction("json", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("json_2");
    ns_web_url ("json_2",
        "URL=https://update.googleapis.com/service/update2/json",
        "METHOD=POST",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chromium","acceptformat":"crx2,crx3","app":[{"appid":"obedbbhbpmojnkanicioggnmelmoomoc","event":[{"download_time_ms":9765,"downloaded":996351,"downloader":"bits","eventresult":1,"eventtype":14,"nextversion":"20210804.389434843","previousversion":"20210717.385693513","total":996351,"url":"http://edgedl.me.gvt1.com/edgedl/release2/chrome_component/hcyekl3dxtzgcfo6ybhibeanny_20210804.389434843/obedbbhbpmojnkanicioggnmelmoomoc_20210804.389434843_all_ENUS_acepkyz5tkv4f3kjbqqqvicsthdq.crx3"},{"eventresult":1,"eventtype":3,"nextfp":"1.092e82c72caa1aa869e6484a3b9adf9b3dafd9d4cc4c6bc2943e56b6affc4ac7","nextversion":"20210804.389434843","previousfp":"1.ce90443029e519576f7a3de55fe2a28284ad3fc73d9f63585bf28d8404ac2ae3","previousversion":"20210717.385693513"}],"version":"20210804.389434843"}],"arch":"x64","dedup":"cr","hw":{"physmemory":12},"lang":"en-US","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.18363.1679"},"prodversion":"90.0.4430.93","protocol":"3.1","requestid":"{485e0c86-8316-44be-a192-6643cca3de7e}","sessionid":"{bd445761-9316-46a3-b2b5-19150e833c14}","updaterversion":"90.0.4430.93"}}",
        BODY_END
    );

    ns_end_transaction("json_2", NS_AUTO_STATUS);
    ns_page_think_time(4.064);

    //Page Auto split for Image Link 'Login' Clicked by User
    ns_start_transaction("login");
    ns_web_url ("login",
        "URL=http://10.10.30.15:9014/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=cavisson&password=cavisson&login.x=0&login.y=0&JSFormSubmit=off",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("login", NS_AUTO_STATUS);
    ns_page_think_time(3.608);

    //Page Auto split for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation");
    ns_web_url ("reservation",
        "URL=http://10.10.30.15:9014/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("reservation", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("json_3");
    ns_web_url ("json_3",
        "URL=https://update.googleapis.com/service/update2/json",
        "METHOD=POST",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chromium","acceptformat":"crx2,crx3","app":[{"appid":"ggkkehgbnfjpeggfpleeakpidbkibbmn","event":[{"download_time_ms":12921,"downloaded":7281,"downloader":"bits","eventresult":1,"eventtype":14,"nextversion":"2021.8.2.1142","previousversion":"2021.7.19.1143","total":7281,"url":"http://storage.googleapis.com/update-delta/ggkkehgbnfjpeggfpleeakpidbkibbmn/2021.8.2.1142/2021.7.19.1143/3e8405d9da8cc381ee72c60850ccdb8a9635ee4e1a907519b8f034a2d7a5dbec.crxd"},{"diffresult":1,"eventresult":1,"eventtype":3,"nextfp":"1.ad9610a41f9aedca740bb5467b5a7ec8e73c0fc55d3427d6f4574eb28dab27fb","nextversion":"2021.8.2.1142","previousfp":"1.74566c4cadcd80a841c10f0636c336a7bb1b94f31a05d7e45ae8b385bfbf53ff","previousversion":"2021.7.19.1143"}],"version":"2021.8.2.1142"}],"arch":"x64","dedup":"cr","hw":{"physmemory":12},"lang":"en-US","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.18363.1679"},"prodversion":"90.0.4430.93","protocol":"3.1","requestid":"{cae491a6-72fc-4b91-b7d1-72498acfbd89}","sessionid":"{bd445761-9316-46a3-b2b5-19150e833c14}","updaterversion":"90.0.4430.93"}}",
        BODY_END,
        INLINE_URLS,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/images/continue.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("json_3", NS_AUTO_STATUS);
    ns_page_think_time(2.095);

    //Page Auto split for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight");
    ns_web_url ("findflight",
        "URL=http://10.10.30.15:9014/cgi-bin/findflight?depart=Acapulco&departDate=08-11-2021&arrive=Acapulco&returnDate=08-12-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=21&findFlights.y=2",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/images/continue.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/images/startover.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("findflight", NS_AUTO_STATUS);
    ns_page_think_time(1.804);

    //Page Auto split for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("findflight_2");
    ns_web_url ("findflight_2",
        "URL=http://10.10.30.15:9014/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C08-11-2021&hidden_outboundFlight_button1=001%7C0%7C08-11-2021&hidden_outboundFlight_button2=002%7C0%7C08-11-2021&hidden_outboundFlight_button3=003%7C0%7C08-11-2021&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=27&reserveFlights.y=19",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/images/startover.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("findflight_2", NS_AUTO_STATUS);
    ns_page_think_time(1.631);

    //Page Auto split for Image Link 'buyFlights' Clicked by User
    ns_start_transaction("findflight_3");
    ns_web_url ("findflight_3",
        "URL=http://10.10.30.15:9014/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=000%7C0%7C08-11-2021&advanceDiscount=&buyFlights.x=65&buyFlights.y=4&.cgifields=saveCC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/images/bookanother.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("findflight_3", NS_AUTO_STATUS);
    ns_page_think_time(1.711);

    //Page Auto split for Image Link 'BookAnother' Clicked by User
    ns_start_transaction("reservation_2");
    ns_web_url ("reservation_2",
        "URL=http://10.10.30.15:9014/cgi-bin/reservation?BookAnother.x=72&BookAnother.y=13",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/images/continue.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=http://10.10.30.15:9014/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("reservation_2", NS_AUTO_STATUS);
    ns_page_think_time(1.741);

}
